#-----------------------------------------
# Variables
#-----------------------------------------
MKFILE_PATH := $(abspath $(lastword ${MAKEFILE_LIST}))
PROJECT_PATH := $(dir ${MKFILE_PATH})
export PROJECT_PATH
PROJECT_NAME := $(shell basename ${PROJECT_PATH})
export PROJECT_NAME
export PROJECT_URL
UID=$(shell id -u)
export UID
GID=$(shell id -g)
export GID

#-----------------------------------------
# Help commands
#-----------------------------------------
.PHONY: help
.DEFAULT_GOAL := help

help: ## Prints this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' ${MAKEFILE_LIST} | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## Gets latest docker images
	@docker compose build --pull

#-----------------------------------------
# Galaxy install
#-----------------------------------------
install: build ## Installs all requirements from Ansible Galaxy
	@docker compose run --rm ansible ansible-galaxy install -f --roles-path .ansible/galaxy-roles -r requirements.yml

playbooks := $(shell find ./playbooks -type f -name '*.yml')
export playbooks
lint:
	@docker compose run --rm ansible /app/lint.sh

#-----------------------------------------
# Workstation
#-----------------------------------------
workstation.req: ## Install requirements for workstation
	@bash ./requirements.sh

workstation.check: workstation.req ## Check configuration of local machine
	@bash ./workstation.sh --check --dif

workstation.apply: workstation.req ## Apply configuration to local machine
	@bash ./workstation.sh

#-----------------------------------------
# Development
#-----------------------------------------
dev.install: install ## Install for development of role!!! Will remove and re-clone role!!
	@rm -rf .ansible/galaxy-roles/roles && git clone git@gitlab.com:dolmen-public/myawesomemachine/roles.git .ansible/galaxy-roles/roles
	@rm -rf .ansible/galaxy-roles/language-tool && git clone git@gitlab.com:dolmen-public/myawesomemachine/language-tool.git .ansible/galaxy-roles/language-tool