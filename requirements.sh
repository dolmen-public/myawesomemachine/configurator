#!/usr/bin/env bash

arg=${@:-""}

set -e
set -u
set -o pipefail

NC='\033[0m'
ZEN_WIDTH=300

function messageOK() {
    echo -e "\033[0;92m✓ $1${NC}"
}

function messageAction() {
    echo -e "\033[0;96m⚑ $1"
}

function messageEnd() {
    echo -e "${NC}"
}

function messageError() {
    echo -e "\033[0;91m✗ $1${NC}"
    zenity --error --text="$1" --width="${ZEN_WIDTH}"
}

function confirm() {
    set +e
    zenity --question --text="${1}" --width="${ZEN_WIDTH}"
    exit_code=$?
    set -e
    if [ "${exit_code}" != "0" ]; then
        messageError "Cancelled by user. Exiting"
        exit 1
    fi
}


if [ "$(whoami)" = root ]; then
    messageError "Please do not run this script as root or using sudo"
    exit 1
fi

## Requirements
# docker
messageAction "Requirements | Docker : checking"
set +e
docker run --rm hello-world 1>/dev/null
exit_code=$?
set -e
if [ "${exit_code}" != "0" ]; then
    confirm 'Docker seems missing or not working properly \n\nDo you want to install it ?'
    messageAction "Installing with https://docs.docker.com/engine/install/ubuntu/"

    messageAction "  [sudo required] Cleaning old binary"
    sudo apt-get remove docker docker-engine docker.io containerd runc || true

    messageAction "  [sudo required] Install requirements"
    sudo apt-get update
    sudo apt-get install -y \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common

    messageAction "  [sudo required] Install GPG key"
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

    messageAction "  [sudo required] Add repository"
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"

    messageAction "  [sudo required] Install docker"
    sudo apt-get update
    sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin

    messageAction "  [sudo required] Post-Install action https://docs.docker.com/engine/install/linux-postinstall/"
    sudo groupadd -f docker || true
    sudo usermod -aG docker "$USER" || true

    messageOK "  Docker install complete"

    zenity --info --text="You need to run \n\n'newgrp docker' \n\nand relaunch this script." --width="${ZEN_WIDTH}"
    exit 10
fi

messageOK "  OK"

# docker compose
messageAction "Requirements | docker compose : checking"
set +e
docker compose version
exit_code=$?
set -e
if [ "${exit_code}" != "0" ]; then
    confirm "docker compose seems missing \n\nDo you want to install it ?"
    messageAction "[sudo required] Install docker compose"
    sudo apt-get update
    sudo apt-get install -y docker-compose-plugin
    messageOK ""
fi
messageOK "  OK"

# Openssh
messageAction "Requirements | sshd : checking"
set +e
which sshd 1>/dev/null
exit_code=$?
set -e
if [ "${exit_code}" != "0" ]; then
    confirm "Openssh-server seems missing. \n\nDo you want to install it"
    messageAction "[sudo required] Install openssh-server"
    sudo apt update && sudo apt install openssh-server
    messageOK ""
fi
messageOK "  OK"
