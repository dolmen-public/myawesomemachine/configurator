#!/usr/bin/env sh
NC='\033[0m'

function messageOK() {
    echo -e "\033[0;32m✓ $1${NC}"
}

function messageAction() {
    echo -e "\033[0;36m⚑ $1"
}

function messageEnd() {
    echo -e "${NC}"
}

function messageError() {
    echo -e "\033[0;31m✗ $1${NC}"
}

playbooks=$(find "./playbooks" -type f -name '*.yml')
messageAction "Syntax check on playbooks"
ansible-playbook --syntax-check "${playbooks}"
messageOK "Syntax checked checked"

messageAction "Lint playbooks and roles"
ansible-lint \
    --force-color "${playbooks}" \
    --exclude "${ANSIBLE_BASE_FOLDER}/galaxy-roles"
messageOK "Lint finished"
