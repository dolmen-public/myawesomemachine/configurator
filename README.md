# Local ansible run

This project is used to run ansible role onto local workstation to set it up.

## TL;DR;

Only the first time, runs 

   ```console
   make install && make workstation.apply
   ```

then the first role (a.k.a workstation) will install a globally available command `workstation`.

## Workstation

### From makefile

To check if your workstation is up to date 

```console
make workstation.check
```

If there are some differences, update your workstation with

```console
make workstation.apply
```

### With command

From anywhere, launch `workstation`, it will :
1. git pull this project
2. Re-install dependencies
3. Runs the requirements
4. Runs configuration in apply mode


## Development

_This is for development of role only._

1. Install for development
   ```bash
   make dev.install
   ```